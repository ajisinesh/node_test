var connect = require("connect");
var http = require("http");

var app = connect();
function error404(res){
    res.writeHead(200,{"content-Type":"text/plain"});
  res.write("error in request");
  res.end();
}

function doFirst(req,res){
  console.log("first function execuetd");
  console.log(req.url);
  if(req.method == 'GET' && req.url == '/'){
   res.writeHead(200,{"content-Type":"text/plain"});
  res.write("first program page");
  res.end();
  }
  else{
    error404(res);
  }
 }
function doSecond(req,res){
  console.log("second function executed");
 res.writeHead(200,{"content-Type":"text/plain"});
  res.write("second program page");
  res.end();
}

app.use('/first',doFirst);
app.use('/second',doSecond);

http.createServer(app).listen(8081);
console.log("server is running on port 8081");
