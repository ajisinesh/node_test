var http = require('http');
var fs = require('fs');
function send404(res){
  res.writeHead(404,{"content-Type":"text/plain"});
  res.write("404:Page Not Found");
  res.end();
}
http.createServer(function(req,res){
  console.log(req.url);
  if(req.method == "GET" && req.url == "/"){
    res.writeHead(200,{'Content-Type':'text/html'});
    fs.createReadStream("./index.html").pipe(res);
  }
  else{
    send404(res);
  }
 }).listen(3002);
console.log('Ajmeer  Server Running at http://127.0.0.1:3002/');
